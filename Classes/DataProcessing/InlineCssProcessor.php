<?php
namespace DigitalZombies\DzBase\DataProcessing;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class InlineCssProcessor
 * @package DigitalZombies\DzBase\DataProcessing
 */
class InlineCssProcessor implements DataProcessorInterface
{

    /**
     * Adds a css file inline to each page
     *
     * @param \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     *
     * @return array the processed data as key/value store
	 * @throws \Exception
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData)
    {
		if(isset($processorConfiguration['fileName'])) {
			$fileName = GeneralUtility::getFileAbsFileName($processorConfiguration['fileName']);

			if(is_file($fileName)) {

				$fh = fopen($fileName, 'r');
				$content = fread($fh, filesize($fileName));

				if($content) {
					/** @var PageRenderer $pageRenderer */
					$pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
					$pageRenderer->addHeaderData('<style>' . $content . '</style>');
				}

				fclose($fh);
			}
		}
        return $processedData;
    }
}
