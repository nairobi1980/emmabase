<?php

namespace Nairobi1980\Emmabase\Domain\Model;

use GeorgRinger\News\Domain\Model\NewsDefault as NewsFields;

class NewsDefault extends NewsFields
{

   /**
   * txEmmabaseLocation.
   * @var string
   */
  protected $txEmmabaseLocation = '';

  public function __construct() {
  }

  /**
   * @return string
   */
  public function getTxEmmabaseLocation()
  {
    return $this->txEmmabaseLocation;
  }

  /**
   * @param string $txEmmabaseLocation
   * @return void
   */
  public function setTxEmmabaseLocation($txEmmabaseLocation)
  {
    $this->txEmmabaseLocation = $txEmmabaseLocation;
  }

  /**
   * txEmmabaseLocation.
   * @var string
   */




   /**
   * txEmmabaseGps.
   * @var string
   */
  protected $txEmmabaseGps = '';

  /**
   * @return string
   */
  public function getTxEmmabaseGps()
  {
    return $this->txEmmabaseGps;
  }

  /**
   * @param string $txEmmabaseGps
   * @return void
   */
  public function setTxEmmabaseGps($txEmmabaseGps)
  {
    $this->txEmmabaseGps = $txEmmabaseGps;
  }

  /**
   * txEmmabaseGps.
   * @var string
   */



  protected $facebook = '';

  /**
   * @return string
   */
  public function getFacebook()
  {
    return $this->facebook;
  }

  /**
   * @param string $facebook
   * @return void
   */
  public function setFacebook($facebook)
  {
    $this->facebook = $facebook;
  }


  protected $website = '';

  /**
   * @return string
   */
  public function getWebsite()
  {
    return $this->website;
  }

  /**
   * @param string $website
   * @return void
   */
  public function setWebsite($website)
  {
    $this->website = $website;
  }

  protected $phone = '';

  /**
   * @return string
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * @param string $phone
   * @return void
   */
  public function setPhone($phone)
  {
    $this->phone = $phone;
  }


  protected $email = '';

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param string $email
   * @return void
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

}