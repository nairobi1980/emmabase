<?php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'rnpbase',
    'Configuration/PageTS/pageTS.tsconfig',
    'Provider Config'
);

call_user_func(
    function ($extKey = 'emmabase') {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/page.tsconfig',
            'DZBase Site'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
            'pages',
            [
                'tx_emmabase_company_logo' => [
                    'l10n_mode' => 'exclude',
                    'label' => 'LLL:EXT:rnpbase/Resources/Private/Language/locallang_db.xlf:tx_rnpbase.pages.tx_emmabase_company_logo',
                    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                        'company_logo',
                        [
                            'overrideChildTca' => [
                                'types' => [
                                    '0' => [
                                        'showitem' => '
									--palette--;;imageoverlayPalette,
									--palette--;;filePalette',
                                    ],
                                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                        'showitem' => '
									--palette--;;imageoverlayPalette,
									--palette--;;filePalette',
                                    ],
                                ],
                                'columns' => [
                                    'crop' => [
                                        'config' => [
                                            'type' => 'imageManipulation',
                                            'cropVariants' => [
                                                'desktop' => [
                                                    'title' => 'Desktop',
                                                    'allowedAspectRatios' => [
                                                        '35:27' => [
                                                            'title' => '35 : 27',
                                                            'value' => 35 / 27,
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'appearance' => [
                                'fileUploadAllowed' => false,
                            ],
                            'maxitems' => 1
                        ]
                    ),
                ],
            ]
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--div--;LLL:EXT:rnpbase/Resources/Private/Language/locallang_db.xlf:tx_rnpbase.pages.styling,
        --linebreak--,tx_rnpbase_menu_type,tx_emmabase_company_logo');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--div--;LLL:EXT:rnpbase/Resources/Private/Language/locallang_db.xlf:tx_rnpbase.pages.contacts,tx_rnpbase_gm_address,
        tx_rnpbase_phone,tx_rnpbase_phone_mobile,tx_rnpbase_email,tx_rnpbase_opening_time,tx_rnpbase_facebook_link,tx_rnpbase_instagram_link,tx_rnpbase_youtube_link');
    }

);