<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey = 'emmabase')
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            $extKey,
            'Configuration/TypoScript',
            'Emma Base'
        );
    }
);

