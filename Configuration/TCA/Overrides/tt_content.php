<?php
defined('TYPO3_MODE') || die('Access denied.');


$GLOBALS['TCA']['tt_content']['types']['mask_our_team_detail']['columnsOverrides']['image']['config']
['overrideChildTca']['columns']['crop']['config']['cropVariants']['desktop'] = [
    'title' => 'Desktop',
    'allowedAspectRatios' => [
        'desktop' => [
            'title' => "3:4",
            'value' => 3 / 4,
        ],
    ]
];