<?php

defined('TYPO3_MODE') || die('Access denied.');

$tmpNewsColumns = [
  'tx_emmabase_location' => [
    'exclude' => 1,
    'label' => 'Location',
    'config' => [
      'type' => 'text',
    ],
  ],
  'tx_emmabase_gps' => [
    'exclude' => 1,
    'label' => 'Coordinates',
    'config' => [
      'type' => 'text',
    ],
  ],
  'facebook' => [
    'exclude' => 1,
    'label' => 'Facebook',
    'config' => [
      'type' => 'text',
    ],
  ],
  'website' => [
    'exclude' => 1,
    'label' => 'Website',
    'config' => [
      'type' => 'text',
    ],
  ],
  'phone' => [
    'exclude' => 1,
    'label' => 'Phone',
    'config' => [
      'type' => 'text',
    ],
  ],
  'email' => [
    'exclude' => 1,
    'label' => 'Email',
    'config' => [
      'type' => 'text',
    ],
  ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $tmpNewsColumns, true);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'facebook', 'website', 'after:teaser');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'tx_emmabase_location','tx_emmabase_gps', '', 'after:teaser');

$GLOBALS['TCA']['tx_news_domain_model_news']['types']['0'] = [
  'showitem' => ' 
                      --palette--;;paletteCore,--palette--;;paletteSlug,title,datetime,archive,
                      --div--;Details,
                      --palette--;Description,teaser,tx_emmabase_size,bodytext,tx_emmabase_location,tx_emmabase_gps, facebook, website, phone, email,
                      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media,fal_media,fal_related_files,
                      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                      categories,
                      --div--;' . $ll . 'tx_news_domain_model_news.tabs.relations,related,
                      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                      --palette--;;paletteHidden,
                      --palette--;;paletteAccess'
];