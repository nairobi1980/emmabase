page = PAGE
page {
    meta {
        description.data = page:description
        keywords.data = page:keywords
        og:site_name = Helyi Infó
        og:site_name.attribute = property
        dc:description.data = page:description
        og:description.data = page:description
        og:locale = {$config.language}
        og:locale.attribute = property
        og:image {
            attribute = property
            stdWrap.cObject = FILES
            stdWrap.cObject {
                references {
                    data = levelfield:-1, media, slide
                }

                maxItems = 1
                renderObj = COA
                renderObj {
                    10 = IMG_RESOURCE
                    10 {
                        file {
                            import.data = file:current:uid
                            treatIdAsReference = 1
                            width = 1280c
                            height = 720c
                        }

                        stdWrap {
                            typolink {
                                parameter.data = TSFE:lastImgResourceInfo|3
                                returnLast = url
                                forceAbsoluteUrl = 1
                            }
                        }
                    }
                }
            }
        }
    }

    typeNum = 0
    10 = FLUIDTEMPLATE
    10 {
        extbase.controllerExtensionName = emmabase
        templateName = TEXT
        templateName.stdWrap {
            cObject = TEXT
            cObject {
                data = levelfield:-2,backend_layout_next_level,slide
                override.field = backend_layout
                split {
                    token = pagets__
                    1.current = 1
                    1.wrap = |
                }
            }

            ifEmpty = DefaultPage
        }

        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            10 {
                levels = 2
                as = navMain
                expandAll = 1
                titleField = nav_title // title
            }

            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                special = directory
                special.value = {$uid.pages.footerMeta}
                levels = 1
                as = navFooterMeta
                expandAll = 1
                titleField = nav_title // title
            }

            30 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            30 {
                special = directory
                special.value = {$uid.pages.usefulLinks}
                levels = 1
                as = navUsefulLinks
                expandAll = 1
                titleField = nav_title // title
            }
        }

        settings < plugin.tx_emmabase.settings

        templateRootPaths {
            10 = EXT:emmabase/Resources/Private/Templates/Page/
        }

        layoutRootPaths {
            10 = EXT:emmabase/Resources/Private/Layouts/
        }

        partialRootPaths {
            10 = EXT:emmabase/Resources/Private/Partials/Page/
        }
    }

    headTag = <head itemscope itemtype="http://schema.org/WebSite">

    headerData {
        1 = COA
        1 {
            wrap = <title itemprop="name">|</title>

            10 = TEXT
            10.data = page:nav_title // page:title
        }

        10 = TEXT
        10.value (
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
        )

        # Meta tags for css information
        11 = TEXT
        11.value (
        )

        22 = TEXT
        22.value = <meta name="dcterms.language" content="{$config.language}" />

        23 = TEXT
        23.data = page:title
        23.wrap = <meta name="dcterms.title" content="|" />

        24 = TEXT
        24.data = page:description
        24.wrap = <meta name="dcterms.description" content="|" />

        25 = TEXT
        25.value (
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
        )

        26 = TEXT
        26.value (
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <script id="eurostat-map" src="https://unpkg.com/eurostat-map"></script>
        )

        33 = TEXT
        33.value = <link rel="preconnect" href="https://cookie.consent.is">

        112 = FLUIDTEMPLATE
        112 {
            file = EXT:emmabase/Resources/Private/Templates/HrefLangAndCanonical.html
            settings < plugin.tx_emmabase.settings
        }
    }

    includeCSS {
        mycss = {$plugin.tx_emmabase.settings.assetsPath}css/styles.min.css
        bootstrap = {$plugin.tx_emmabase.settings.cssPath}/bootstrap.min.css
        fa = {$plugin.tx_emmabase.settings.cssPath}/all.min.css
        sli = {$plugin.tx_emmabase.settings.cssPath}/simple-line-icons.css
        slick = {$plugin.tx_emmabase.settings.cssPath}/slick.css
        owl = {$plugin.tx_emmabase.settings.cssPath}/owl.carousel.min.css
    }

    headerData.116 = TEXT
    headerData.116.value(
      <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    )

    includeJSFooter {

        popper = {$plugin.tx_emmabase.settings.jsPath}/popper.min.js
        bootstrap = {$plugin.tx_emmabase.settings.jsPath}/bootstrap.min.js
        sticky = {$plugin.tx_emmabase.settings.jsPath}/jquery.sticky-sidebar.min.js
        #custom = {$plugin.tx_emmabase.settings.jsPath}/custom.js
        scripts = {$plugin.tx_emmabase.settings.assetsPath}js/scripts.min.js
    }

    footerData{ 
      999999 = TEXT
      999999.value(
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfG-SYgJOdwjaG6Eu6dSnxLaAy7Xic2Oo&callback=initListMap&&libraries=places&v=weekly"
       ></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
      )
    }

    headerData{
      101 = TEXT
      101.value(

      )

    }

    jsFooterInline {
    }

    variables {
        Login = USER_INT
            Login {
                userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
                controllerActionName = login
                controllerExtensionName = Felogin
                controllerName = Login
                pluginName = Login
                extensionName = Felogin
                vendorName = TYPO3

                view {
                    layoutRootPaths {
                        10 = YourPath/Private/Extensions/felogin/Layouts/
                    }

                    templateRootPaths {
                        10 = YourPath/Private/Extensions/felogin/Templates/
                    }

                    partialRootPaths {
                        10 = YourPath/Private/Extensions/felogin/Partials/
                    }
                }

                settings {
                    storagePid = 4
                    pages = 2
                    showForgotPassword = 0
                    showLogoutFormAfterLogin = 1
                    redirectMode = userLogin,groupLogin
                    redirectFirstMethod = 1
                    redirectPageLogin =
                    redirectPageLoginError = 27
                    redirectPageLogout = 27
                    redirectDisable = 0
                    error_header = Fehler
                    success_header = Success
                    success_message = Login erfolgreich
                    showForgotPassword = 0
                }
            }
      }
  }
plugin.tx_felogin_pi1.showLogoutFormAfterLogin = 1
