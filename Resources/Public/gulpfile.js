'use strict';

const gulp = require('gulp');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const beautify = require('gulp-beautify');
const sourcemaps = require('gulp-sourcemaps');
const del = require("del");
const gulpclean = require('gulp-clean-css');
const eslint = require("gulp-eslint");
const plumber = require("gulp-plumber");
const sassLint = require('gulp-sass-lint');
const sass = require('gulp-sass');
const size = require('gulp-size');
const imagemin = require('gulp-imagemin');
const webp = require('gulp-webp');

//Frontend directories
let sourceDirectory = "src/";
let destinationDirectory = "dist/";

//Backend preview styles
let sourceBackendPreviewDirectory = "Stylesheet/Backend/src_styles";
let destinationBackendPreviewDirectory = "Stylesheet/Backend/styles";

//Backend preview scripts
let sourceBackendPreviewScriptsDirectory = "JavaScript/src/";
let destinationBackendPreviewScriptsDirectory = "JavaScript/";

// Clean assets directory.
function directoryCleaner() {
    return del([destinationBackendPreviewDirectory + "/*", destinationDirectory + "css/*", destinationDirectory + "js/*", destinationDirectory + "fonts/*", destinationDirectory + "images/*"]);
}

function cssDirectoryCleaner() {
    return del(destinationDirectory + "css/*");
}

function cssBackendDirectoryCleaner() {
    return del(destinationBackendPreviewDirectory + "/*");
}

function jsDirectoryCleaner() {
    return del(destinationDirectory + "js/*");
}

function jsBackendDirectoryCleaner() {
    return del(destinationBackendPreviewScriptsDirectory + "Scripts.min.js");
}

function fontsDirectoryCleaner() {
    return del(destinationDirectory + "fonts/*");
}

function imagesDirectoryCleaner() {
    return del(destinationDirectory + "images/*");
}

// Watch files for changes.
function filesWatcher(cb) {
    gulp.watch(sourceDirectory + "scss/**/*.scss", stylesBuild);
    gulp.watch(sourceBackendPreviewDirectory + "/**/*.scss", stylesBackendBuild);
    gulp.watch(sourceBackendPreviewScriptsDirectory + "/**/*.js", scriptsBackendBuild);
    gulp.watch(sourceDirectory + "js/**/*.js", scriptsBuild);
    gulp.watch(sourceDirectory + "fonts/**/*", fontsBuild);
    gulp.watch(sourceDirectory + "images/**/*", mediaBuild);
    cb();
}

// Linter for scripts files.
let scriptsSource = [sourceDirectory + "js/**/*", "./gulpfile.js"];

function scriptsLinter() {
    return gulp
        .src(scriptsSource)
        .pipe(plumber())
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
}

// Linter for scss files.
let stylesSource = sourceDirectory + "scss/**/*.scss";

function stylesLinter() {
    return gulp.src(stylesSource)
        .pipe(size({
            showFiles: true,
            title: 'SCSS  '
        }))
        .pipe(sassLint({
            config: '.sass-lint.yml'
        }))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError());
}

// Javascript minimizer.
let jsSource = [
    sourceDirectory + 'js/jQuery-3.4.1.min.js',
    sourceDirectory + 'js/jquery-migrate.js',
    sourceDirectory + 'js/magnific-popup.min.js',
    sourceDirectory + 'js/waypoints.min.js',
    sourceDirectory + 'js/waypoints.min.js',
    sourceDirectory + 'js/waypoints-sticky.min.js',
    sourceDirectory + 'js/counterup2.js',
    sourceDirectory + 'js/owl.carousel-2.3.4.min.js',
    sourceDirectory + 'js/stellar.min.js',
    sourceDirectory + 'js/scrolla.min.js',
    sourceDirectory + 'js/isotope.pkgd.min.js',
    sourceDirectory + 'js/custom.js',
    sourceDirectory + 'js/lazysizes.min.js',
    sourceDirectory + 'js/ls.noscript.min.js',
    sourceDirectory + 'js/myscript.js',
    sourceDirectory + 'js/chart.js',
];

let jsDestination = destinationDirectory + 'js';

function scriptsMinimizer() {
    return gulp.src(jsSource)
        .pipe(plumber())
        .pipe(size({
            showFiles: true,
            title: 'File size before compression of '
        }))
        .pipe(concat('scripts.js'))
        .pipe(rename('scripts.min.js'))
        .pipe(size({
            showFiles: true,
            title: 'File size of '
        }))
        .pipe(gulp.dest(jsDestination));
}

// Backend Javascript minimizer.
let jsSourceBackend = sourceBackendPreviewScriptsDirectory + 'Scripts.js';
let jsDestinationBackend = destinationBackendPreviewScriptsDirectory;

function scriptsBackendMinimizer() {
    return gulp.src(jsSourceBackend)
        .pipe(plumber())
        .pipe(size({
            showFiles: true,
            title: 'File size before compression of '
        }))
        .pipe(concat('Scripts.js'))
        .pipe(uglify())
        .pipe(rename('Scripts.min.js'))
        .pipe(size({
            showFiles: true,
            title: 'File size of '
        }))
        .pipe(gulp.dest(jsDestinationBackend));
}

// Unminify specified script
//let unminifySrc = sourceDirectory + "/js/revolution/jquery.themepunch.revolution.min.js";
//let jsDestinationUnminify = sourceDirectory + '/js/revolution/jquery.themepunch.revolution.js';

function unminimizeScripts() {
    return gulp.src(unminifySrc)
        .pipe(beautify.js({
            indent_size: 2
        }))
        .pipe(rename('Core.js'))
        .pipe(gulp.dest(jsDestinationUnminify))
}

// SCSS frontend styles compiler.
let scssSource = [
    sourceDirectory + 'css/font-awesome.min.css',
    sourceDirectory + "scss/*.scss"
];

let scssDestination = destinationDirectory + 'css';

function stylesCompiler() {
    return gulp.src(scssSource)
        .pipe(size({
            showFiles: true,
            title: 'SCSS  '
        }))
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .on("error", sass.logError)
        .pipe(concat('styles.css'))
        .pipe(gulpclean({
            compatibility: 'ie11'
        }))
        .pipe(rename('styles.min.css'))
        .pipe(size({
            showFiles: true,
            title: 'File size of '
        }))
        .pipe(gulp.dest(scssDestination));
}

let scssContentSource = sourceDirectory + 'scss/contents/**/*.scss';
let scssSourceDestination = destinationDirectory + '/contents';

function contentStylesCompiler() {
    return gulp.src(scssContentSource)
        .pipe(size({
            showFiles: true,
            title: 'SCSS  '
        }))
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .on("error", sass.logError)
        .pipe(gulpclean({
            compatibility: 'ie11'
        }))
        .pipe(size({
            showFiles: true,
            title: 'File size of '
        }))
        .pipe(gulp.dest(scssSourceDestination));
}

//SCSS backend styles compiler
let scssBackendSource = sourceBackendPreviewDirectory + "/Style.scss";

function stylesBackendCompiler() {
    return gulp.src(scssBackendSource)
        .pipe(sourcemaps.init({
            largeFile: true
        }))
        .pipe(size({
            showFiles: true,
            title: 'File size before compile of '
        }))
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .on("error", sass.logError)
        .pipe(concat('Style.css'))
        .pipe(gulpclean())
        .pipe(sourcemaps.write())
        .pipe(rename('Style.min.css'))
        .pipe(size({
            showFiles: true,
            title: 'File size of '
        }))
        .pipe(gulp.dest(destinationBackendPreviewDirectory));
}

// Media optimization for frontend.
let imagesSource = sourceDirectory + 'images/**/*';
let imagesDestination = destinationDirectory + 'images';

function mediaOptimization() {
    return gulp.src(imagesSource)
        .pipe(imagemin([
            imagemin.gifsicle({
                interlaced: true
            }),
            imagemin.jpegtran({
                progressive: true
            }),
            imagemin.optipng({
                optimizationLevel: 5
            }),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                },
                {
                    cleanupIDs: false
                },
                {
                    convertStyleToAttrs: false
                },
                {
                    removeEmptyContainers: false
                },
                {
                    mergePaths: false
                },
                {
                    minifyStyles: false
                }
                ]
            })
        ]))
        .pipe(gulp.dest(imagesDestination));
}

// Font file exporter.
let fontSource = sourceDirectory + 'fonts/*';
let fontDestination = destinationDirectory + 'fonts';

function fontExporter() {
    return gulp.src(fontSource)
        .pipe(gulp.dest(fontDestination))
}

// Scritps  exporter.
let scriptSource = [
    sourceDirectory + 'js/html5shiv.min.js',
    sourceDirectory + 'js/respond.min.js'
];
let scriptDestination = destinationDirectory + 'js';
/* 
let scriptSourceRevolution = [
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.actions.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.carousel.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.kenburn.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.layeranimation.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.migration.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.navigation.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.parallax.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.slideanims.min.js',
    sourceDirectory + 'js/revolution/js/extensions/revolution.extension.video.min.js'
]; */

//let scriptDestinationRevolution = destinationDirectory + 'js/revolution/js/extensions';

// Scritps exporters.
function scriptsExporter() {
    return gulp.src(scriptSource)
        .pipe(gulp.dest(scriptDestination))
}

function scriptsRevolutionExporter() {
    return gulp.src(scriptSourceRevolution)
        .pipe(gulp.dest(scriptDestinationRevolution))
}

// Webp converter
let fileadminSource = '../../../../../fileadmin/**/*.{jpg,JPG,jpeg,png}';
let fileadminDest = '../../../../../fileadmin/';

function webpConverter() {
    return gulp.src(fileadminSource)
        .pipe(rename((function (path) {
            path.extname += path.extname;
        })))
        .pipe(webp())
        .pipe(gulp.dest(fileadminDest));
}

// Webp converter
let pluginImagesSource = sourceDirectory + 'images/**/*.{jpg,JPG,jpeg,png}';
let pluginImagesDest = destinationDirectory + 'images';

// Webp converter for plugin images
function webpPluginImagesConverter() {
    return gulp.src(pluginImagesSource)
        .pipe(rename((function (path) {
            path.extname += path.extname;
        })))
        .pipe(webp())
        .pipe(gulp.dest(pluginImagesDest));
}

// Defined tasks.
const scriptsBuild = gulp.series(jsDirectoryCleaner, scriptsLinter, scriptsExporter, scriptsMinimizer);
const scriptsBackendBuild = gulp.series(jsBackendDirectoryCleaner, scriptsLinter, scriptsBackendMinimizer);
const stylesBuild = gulp.series(cssDirectoryCleaner, stylesLinter, stylesCompiler, contentStylesCompiler);
const stylesBackendBuild = gulp.series(cssBackendDirectoryCleaner, stylesBackendCompiler);
const mediaBuild = gulp.series(imagesDirectoryCleaner, mediaOptimization, webpPluginImagesConverter);
const fontsBuild = gulp.series(fontsDirectoryCleaner, fontExporter);
const build = gulp.series(directoryCleaner, stylesBuild, stylesBackendBuild, scriptsBuild, scriptsBackendBuild, mediaBuild, fontExporter);
const watchFiles = gulp.parallel(filesWatcher);
const webpConv = gulp.series(webpConverter);

// Exported tasks.
exports.styles = stylesBuild;
exports.bstyles = stylesBackendBuild;
exports.scripts = scriptsBuild;
exports.bscripts = scriptsBackendBuild;
exports.uscripts = unminimizeScripts;
exports.clean = directoryCleaner;
exports.media = mediaBuild;
exports.fonts = fontsBuild;
exports.build = build;
exports.watch = watchFiles;
exports.default = build;
exports.webpConverter = webpConverter;