(function ($) {

    'use strict';
    /*--------------------------------------------------------------------------------------------
        document.ready ALL FUNCTION START
    ---------------------------------------------------------------------------------------------*/

// > Main menu sticky on top  when scroll down function by = custom.js ========== //

    function sticky_header() {
        if (jQuery('.sticky-header').length) {
            var sticky = new Waypoint.Sticky({
                element: jQuery('.sticky-header')
            })
        }
    }

// > page scroll top on button click function by = custom.js ===================== //	

    function scroll_top() {
        jQuery("button.scroltop").on('click', function () {
            jQuery("html, body").animate({
                scrollTop: 0
            }, 1000);
            return false;
        });

        jQuery(window).on("scroll", function () {
            var scroll = jQuery(window).scrollTop();
            if (scroll > 900) {
                jQuery("button.scroltop").fadeIn(1000);
            } else {
                jQuery("button.scroltop").fadeOut(1000);
            }
        });
    }

// > accordion active calss function by = custom.js ========================= //	

    function accordion_active() {
        $('.acod-head a').on('click', function () {
            $('.acod-head').removeClass('acc-actives');
            $(this).parents('.acod-head').addClass('acc-actives');
            $('.acod-title').removeClass('acc-actives'); //just to make a visual sense
            $(this).parent().addClass('acc-actives'); //just to make a visual sense
            ($(this).parents('.acod-head').attr('class'));
        });
    }

// > Nav submenu show hide on mobile by = custom.js

    function mobile_nav() {
        jQuery(".sub-menu").parent('li').addClass('has-child');
        jQuery(".mega-menu").parent('li').addClass('has-child');
        jQuery("<div class=' glyphicon glyphicon-plus submenu-toogle'></div>").insertAfter(".has-child > a");
        jQuery('.has-child a+.submenu-toogle').click(function (ev) {
            jQuery(this).next(jQuery('.sub-menu')).toggleClass('sub-menu-active');
            jQuery(this).parent().toggleClass('nav-active');
            ev.stopPropagation();
        });
    }

// > blog Carousel_1 Full Screen with no margin function by = owl.carousel.js ========================== //

    function client_logo_carousel_1() {
        jQuery('.client-logo-carousel').owlCarousel({
            loop: true,
            autoplay:true,
            autoplayTimeout:4000,
            margin: 10,
            nav: false,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                767: {
                    items: 3
                },
                1000: {
                    items: 4
                },
                1200: {
                    items: 5
                }
            }
        });
    }

    /*--------------------------------------------------------------------------------------------
        Window on load ALL FUNCTION START
    ---------------------------------------------------------------------------------------------*/

// > background image parallax function by = stellar.js ==================== //

    function bg_image_stellar() {
        jQuery(function () {
            jQuery.stellar({
                horizontalScrolling: false,
                verticalOffset: 100
            });
        });
    }

    /*--------------------------------------------------------------------------------------------
        Window on scroll ALL FUNCTION START
    ---------------------------------------------------------------------------------------------*/

    function color_fill_header() {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            $(".is-fixed").addClass("color-fill");
        } else {
            $(".is-fixed").removeClass("color-fill");
        }
    };

    /*--------------------------------------------------------------------------------------------
        document.ready ALL FUNCTION START
    ---------------------------------------------------------------------------------------------*/
    jQuery(document).ready(function () {
        // > Main menu sticky on top  when scroll down function by = custom.js
        sticky_header(),
            // > page scroll top on button click function by = custom.js
            scroll_top(),
            // > accordion active calss function by = custom.js ========================= //
            accordion_active(),
            // > Nav submenu on off function by = custome.js ===================//
            mobile_nav(),
            //  blog Carousel_1 Full Screen with no margin function by = owl.carousel.js ========================== //
            client_logo_carousel_1()
    });

    /*--------------------------------------------------------------------------------------------
        Window Load START
    ---------------------------------------------------------------------------------------------*/
    jQuery(window).on('load', function () {
            // > background image parallax function by = stellar.js
            bg_image_stellar()
    });

    /*===========================
       Window Scroll ALL FUNCTION START
   ===========================*/

    jQuery(window).on('scroll', function () {
        // > Window on scroll header color fill
        color_fill_header()
    });

})(window.jQuery);