(function ($) {
    'use strict';

    // > magnificPopup function	by = magnific-popup.js =========================== //

    function magnific_popup() {
        jQuery('.mfp-gallery').magnificPopup({
            delegate: '.mfp-link',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            }
        });
    }

// > magnificPopup for video function	by = magnific-popup.js ===================== //

    function magnific_video() {
        jQuery('.mfp-video').magnificPopup({
            type: 'iframe',
        });
    }

// > Main menu sticky on top  when scroll down function by = custom.js ========== //

    function sticky_header() {
        if (jQuery('.sticky-header').length) {
            var sticky = new Waypoint.Sticky({
                element: jQuery('.sticky-header')
            })
        }
    }

// > page scroll top on button click function by = custom.js ===================== //	

    function scroll_top() {
        jQuery("button.scroltop").on('click', function () {
            jQuery("html, body").animate({
                scrollTop: 0
            }, 1000);
            return false;
        });

        jQuery(window).on("scroll", function () {
            var scroll = jQuery(window).scrollTop();
            if (scroll > 900) {
                jQuery("button.scroltop").fadeIn(1000);
            } else {
                jQuery("button.scroltop").fadeOut(1000);
            }
        });
    }

// > Nav submenu show hide on mobile by = custom.js

    function mobile_nav() {
        jQuery(".sub-menu").parent('li').addClass('has-child');
        jQuery(".mega-menu").parent('li').addClass('has-child');
        jQuery("<div class=' glyphicon glyphicon-plus submenu-toogle'></div>").insertAfter(".has-child > a");
        jQuery('.has-child a+.submenu-toogle').click(function (ev) {
            jQuery(this).next(jQuery('.sub-menu')).toggleClass('sub-menu-active');
            jQuery(this).parent().toggleClass('nav-active');
            ev.stopPropagation();
        });
    }

    /*--------------------------------------------------------------------------------------------
        Window on scroll ALL FUNCTION START
    ---------------------------------------------------------------------------------------------*/

    function color_fill_header() {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            $(".is-fixed").addClass("color-fill");
        } else {
            $(".is-fixed").removeClass("color-fill");
        }
    };

    /*--------------------------------------------------------------------------------------------
        document.ready ALL FUNCTION START
    ---------------------------------------------------------------------------------------------*/
    jQuery(document).ready(function () {
            // > magnificPopup function	by = magnific-popup.js
            magnific_popup(),
            // > magnificPopup for video function	by = magnific-popup.js
            magnific_video(),
            // > Main menu sticky on top  when scroll down function by = custom.js
            sticky_header(),
            // > page scroll top on button click function by = custom.js
            scroll_top(),
            // > Nav submenu on off function by = custome.js ===================//
            mobile_nav()
    });

    /*===========================
       Window Scroll ALL FUNCTION START
   ===========================*/

    jQuery(window).on('scroll', function () {
        // > Window on scroll header color fill
        color_fill_header()
    });

})(window.jQuery);