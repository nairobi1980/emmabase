<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Emma Base Site',
    'description' => 'Site package for Emma Base',
    'category' => 'templates',
    'author' => 'Nagy Róbert',
    'author_email' => 'robertnagy@outlook.com',
    'author_company' => 'Nagy Róbert Bt.',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '1.0.0',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '10.4.0-10.4.99',
                    'news' => '8.2.8-10.0.0'
                ],
            'conflicts' =>
                [
                ],
            'suggests' =>
                [
                ],
        ],
];

