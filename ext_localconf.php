<?php

defined('TYPO3_MODE') || die('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/NewsDefault'][] = 'emmabase';

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default'] = 'EXT:emmabase/Configuration/RTE/Default.yaml';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    "@import 'EXT:emmabase/Configuration/TSconfig/Page/Basic.tsconfig'
  @import 'EXT:emmabase/Configuration/TSconfig/Page/TCEFORM.tsconfig'"
);

/* $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:cms/locallang_tca.xlf'][] = 'EXT:examples/Resources/Private/Language/custom.xlf'; */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['hu']['EXT:sf_register/Resources/Private/Language/locallang.xlf'][] = 'EXT:emmabase/Resources/Private/Language/Overrides/hu.locallang.xlf';
