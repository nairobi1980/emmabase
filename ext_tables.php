<?php

defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile("emmabase", 'Configuration/TypoScript', 'News new fields');

$tempColumns = Array (
                "facebook" => Array (
                                "exclude" => 1,
                                "label" => "facebook",
                                "config" => Array (
                                                "type" => "input",
                                                "size" => "60",
                                                "eval" => "trim",
                                )
                ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns("tx_news_domain_model_news",$tempColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("tx_news_domain_model_news","facebook;;;;1-1-1");
?>
