CREATE TABLE pages
(
  /*  -- Styling --  */
 /*  tx_emmabase_font_size int(11) DEFAULT '0' NOT NULL,
  tx_emmabase_font_family int(11) DEFAULT '0' NOT NULL,
  tx_emmabase_theme int(11) DEFAULT '0' NOT NULL,
  tx_emmabase_menu_type int(11) DEFAULT '0' NOT NULL, */
  tx_emmabase_company_logo int(11) DEFAULT '0' NOT NULL,

  /*  -- Contacts--  */
  tx_emmabase_address varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_gm_address varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_phone varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_phone_mobile varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_email varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_opening_time varchar(255) DEFAULT '' NOT NULL,

  /*  -- Social Links--  */
  tx_emmabase_facebook_link varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_instagram_link varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_youtube_link varchar(255) DEFAULT '' NOT NULL,
);


CREATE TABLE tx_news_domain_model_news
(
  tx_emmabase_location varchar(255) DEFAULT '' NOT NULL,
  tx_emmabase_gps varchar(255) DEFAULT '' NOT NULL,
  facebook varchar(255) DEFAULT '' NOT NULL,
  phone varchar(255) DEFAULT '' NOT NULL,
  email varchar(255) DEFAULT '' NOT NULL,
  website varchar(255) DEFAULT '' NOT NULL,
);